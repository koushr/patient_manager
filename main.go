package main

import (
	"fmt"
	"net/http"
)

func main() {
	requests := 10000
	ch := make(chan string, requests)
	for i := 1; i <= requests; i++ {
		go func(iter int) {
			fmt.Println(iter)
			resp, _ := http.Get("http://localhost:8080/api/project")
			resp.Body.Close()
			ch <- fmt.Sprint("%i", iter)
		}(i)
	}
	for i := 1; i <= requests; i++ {
		fmt.Println(<-ch)
	}
}
